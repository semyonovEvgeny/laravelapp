<?php declare(strict_types=1);

namespace App\Services;


use App\Services\DataProviders\Middleware\OGraphMiddleware;
use App\Services\DataProviders\Middleware\TwitterMiddleware;
use Illuminate\Pipeline\Pipeline;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class MetaTagsDataProvider
 * @package App\Services
 */
class MetaTagsDataProvider
{
    /**
     * @var array
     */
    protected $nodes = ['description', 'title', 'image', 'url', 'card'];

    /**
     * @param string $url
     * @return array
     */
    public function getBody(string $url):array
    {
        $html = file_get_contents($url);
        $crawler = new Crawler(null, $url);
        $crawler->addHtmlContent($html, 'UTF-8');

        $pipes = [
            OGraphMiddleware::class,
            TwitterMiddleware::class
        ];

        $dataObj= (object) ['crawler'=> $crawler,'data' => [],'keys'=> $this->nodes];
        $finalData = app(Pipeline::class)
            ->send($dataObj)
            ->through($pipes)
            ->via('handle')
            ->then(function ($changedData) {
                return $changedData;
            });
        return $finalData->data;
    }
}
