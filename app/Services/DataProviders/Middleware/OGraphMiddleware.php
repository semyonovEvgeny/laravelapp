<?php declare(strict_types=1);


namespace App\Services\DataProviders\Middleware;


use Closure;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class OGraphMiddleware
 * @package App\Services\DataProviders\Middleware
 */
class OGraphMiddleware extends AbstractMiddleware
{
    /**
     * @param object $dataObj
     * @param Closure $next
     * @return mixed|object|void
     */
    public function handle(object $dataObj, Closure $next)
    {
        foreach ($dataObj->keys as $key) {
            $dataObj->data[$key] = $dataObj->crawler
                ->filterXPath("//meta[@property='og:$key']")
                ->extract(['content']);
        }

        if($this->arrayCheck($dataObj->data)){
            return $next($dataObj);
        }
        return $dataObj;
    }
}
