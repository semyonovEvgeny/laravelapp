<?php declare(strict_types=1);


namespace App\Services\DataProviders\Middleware;


use Closure;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class AbstractMiddleware
 * @package App\Services\DataProviders\Middleware
 */
class AbstractMiddleware
{
    /**
     * @param object $dataObj
     * @param Closure $next
     */
    public function handle(object $dataObj, Closure $next)
    {

    }

    /**
     * @param array $data
     * @return bool
     */
    protected function arrayCheck(array $data):bool
    {
        foreach ($data as $key=>$value) {
            if(!isset($data[$key])) {
                return false;
            }
            return true;
        }
    }
}
