<?php declare(strict_types=1);


namespace App\Services\DataProviders\Middleware;


use Closure;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class TwitterMiddleware
 * @package App\Services\DataProviders\Middleware
 */
class TwitterMiddleware extends AbstractMiddleware
{
    /**
     * @param object $dataObj
     * @param Closure $next
     * @return mixed|object|void
     */
    public function handle(object $dataObj, Closure $next)
    {
        foreach ($dataObj->keys as $key) {
            if($dataObj->data[$key] ==[]) {
                $dataObj->data[$key] = $dataObj->crawler
                    ->filterXPath("//meta[@name='twitter:$key']")
                    ->extract(['content']);
            }
        }
        if($this->arrayCheck($dataObj->data)){
            return $next($dataObj);
        }
        return $dataObj;
    }
}
