<?php

use App\Services\MetaTagsDataProvider;
use Illuminate\Http\Request;

/**
 * @var $router Illuminate\Routing\Router;
 */

$router->get("/", function (Request $request){
    return response()
        ->json(
            (new MetaTagsDataProvider())
                ->getBody($request['url'])
        );
});
